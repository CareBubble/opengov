<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/app/{id}', function () {
    return view('apps.show');
});
Route::get('/about', function () {
    return view('pages.about');
});
Route::get('/cart', function () {
    return view('shop.cart');
});
Route::get('/guide', function () {
    return view('pages.guide');
});
Route::get('/page/legal/licenses', function () {
    return view('pages.licenses');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
