@extends('layouts.app')

@section('content')
    <section class="hero">
        <!-- Large header banner -->
        <div class="container">
            <img
                src="./theme_files/bootstrap-stack.png"
                srcset="
                    https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/elements/bootstrap-stack.png    1x,
                    https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/elements/bootstrap-stack@2x.png 2x
                "
            />
            <h1 class="display-1 text-bold">Upgrade your Gov Tech</h1>
            <h5 class="text-gray-soft text-regular">Bring your Town, City or maybe even Country into the 21st centery with Open Source software for governing.</h5>
            <a class="button button-brand btn-lg mb-5 mb-lg-2" href="/about">Why our Apps?</a>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <div class="theme-cards-holder">
                <div class="theme-cards__heading">
                    <div>
                        <h5 class="theme-cards__title">Latest</h5>
                        <p class="text-gray-soft">Most recently added to our collection.</p>
                    </div>
                    <a class="theme-cards__heading__button btn btn-outline-brand btn-sm" href="https://themes.getbootstrap.com/shop/?orderby=date">View all</a>
                </div>
                <ul class="row">
                    <li class="col-6">
                        <div class="theme-card">
                            <div class="theme-card__body">
                                <a class="d-block" href="/app/1">
                                    <img
                                        src="https://images.squarespace-cdn.com/content/v1/5a4f79d6aeb625d6f842c5d5/1594629947980-VBLNWPU2CYWQ7O41UQ89/X-Road%252Becosystem.jpg?"
                                    />
                                </a>
                                <a class="theme-card__body__overlay btn btn-brand btn-sm" target="_blank" href="/apps/1">Live preview</a>
                            </div>
                            <div class="theme-card__footer">
                                <div class="theme-card__footer__item">
                                    <a class="theme-card__title mr-1" href="/apps/1">GovNet - The fast, reliable and interoppale base layer all the apps run on</a>
                                    <p class="theme-card__info"></p>
                                    <ul class="prod_cats_list">
                                        <li><a href="https://themes.getbootstrap.com/product-category/portfolio-blog/">Blockchain &amp; Currency</a></li>
                                    </ul>
                                    <p></p>
                                </div>
                                <div class="theme-card__footer__item">
                                    <p class="theme-card__price">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>49.00</span>
                                    </p>
                                    <ul class="rating">
                                        <li class="rating__item"></li>
                                        <li class="rating__item"></li>
                                        <li class="rating__item"></li>
                                        <li class="rating__item"></li>
                                        <li class="rating__item"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="col-6">
                        <div class="theme-card">
                            <div class="theme-card__body">
                                <a class="d-block" href="https://themes.getbootstrap.com/product/finder-directory-listings-template-ui-kit/">
                                    <img
                                        data-src="https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-540x405.jpg"
                                        data-srcset="https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-800x600.jpg 800w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot.jpg 1200w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-768x576.jpg 768w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-600x450.jpg 600w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-200x150.jpg 200w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-400x300.jpg 400w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-540x405.jpg 540w"
                                        data-sizes="auto"
                                        class="theme-card__img lazyautosizes lazyloaded"
                                        sizes="540px"
                                        srcset="
                                            https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-800x600.jpg  800w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot.jpg         1200w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-768x576.jpg  768w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-600x450.jpg  600w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-200x150.jpg  200w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-400x300.jpg  400w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-540x405.jpg  540w
                                        "
                                        src="./theme_files/screenshot-540x405.jpg"
                                    />
                                </a>
                                <a class="theme-card__body__overlay btn btn-brand btn-sm" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=92316">Live preview</a>
                            </div>
                            <div class="theme-card__footer">
                                <div class="theme-card__footer__item">
                                    <a class="theme-card__title mr-1" href="https://themes.getbootstrap.com/product/finder-directory-listings-template-ui-kit/">Finder – Directory &amp; Listings Template + UI Kit</a>
                                    <p class="theme-card__info"></p>
                                    <ul class="prod_cats_list">
                                        <li><a href="https://themes.getbootstrap.com/product-category/ecommerce-retail/">E-Commerce &amp; Retail</a></li>
                                    </ul>
                                    <p></p>
                                </div>
                                <div class="theme-card__footer__item">
                                    <p class="theme-card__price">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>49.00</span>
                                    </p>
                                    <ul class="rating">
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <a class="btn btn-brand btn-block d-md-none" href="https://themes.getbootstrap.com/shop/?orderby=date">View all latest themes</a>
            </div>
            <div class="theme-cards-holder">
                <div class="theme-cards__heading">
                    <div>
                        <h5 class="theme-cards__title">Popular</h5>
                        <p class="text-gray-soft">Top-sellers in the past week!</p>
                    </div>
                    <!-- Removed for now because we're listing all the themes directly on the homepage -->
                    <!-- <a class="theme-cards__heading__button btn btn-outline-brand btn-sm" href="https://themes.getbootstrap.com/shop/?orderby=popularity">View all</a> -->
                </div>
                <ul class="row">
                    <li class="col-6">
                        <div class="theme-card">
                            <div class="theme-card__body">
                                <a class="d-block" href="https://themes.getbootstrap.com/product/hyper-responsive-admin-dashboard-template/">
                                    <img
                                        data-src="https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-540x405.png"
                                        data-srcset="https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-800x600.png 800w, https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3.png 1200w, https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-768x576.png 768w, https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-600x450.png 600w, https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-200x150.png 200w, https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-400x300.png 400w, https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-540x405.png 540w"
                                        data-sizes="auto"
                                        class="theme-card__img lazyautosizes lazyloaded"
                                        sizes="540px"
                                        srcset="
                                            https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-800x600.png  800w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3.png         1200w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-768x576.png  768w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-600x450.png  600w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-200x150.png  200w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-400x300.png  400w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2018/07/hyper-shot-1200x900-3-540x405.png  540w
                                        "
                                        src="./theme_files/hyper-shot-1200x900-3-540x405.png"
                                    />
                                </a>
                                <a class="theme-card__body__overlay btn btn-brand btn-sm" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=6743">Live preview</a>
                            </div>
                            <div class="theme-card__footer">
                                <div class="theme-card__footer__item">
                                    <a class="theme-card__title mr-1" href="https://themes.getbootstrap.com/product/hyper-responsive-admin-dashboard-template/">Hyper – Admin &amp; Dashboard Template (Dark/Light)</a>
                                    <p class="theme-card__info"></p>
                                    <ul class="prod_cats_list">
                                        <li><a href="https://themes.getbootstrap.com/product-category/admin-dashboard/">Admin &amp; Dashboard</a></li>
                                    </ul>
                                    <p></p>
                                </div>
                                <div class="theme-card__footer__item">
                                    <p class="theme-card__price">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>49.00</span>
                                    </p>
                                    <ul class="rating">
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="col-6">
                        <div class="theme-card">
                            <div class="theme-card__body">
                                <a class="d-block" href="https://themes.getbootstrap.com/product/falcon-admin-dashboard-webapp-template/">
                                    <img
                                        data-src="https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-540x405.jpeg"
                                        data-srcset="https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-800x600.jpeg 800w, https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2.jpeg 1200w, https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-768x576.jpeg 768w, https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-600x450.jpeg 600w, https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-200x150.jpeg 200w, https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-400x300.jpeg 400w, https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-540x405.jpeg 540w"
                                        data-sizes="auto"
                                        class="theme-card__img lazyautosizes lazyloaded"
                                        sizes="540px"
                                        srcset="
                                            https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-800x600.jpeg  800w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2.jpeg         1200w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-768x576.jpeg  768w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-600x450.jpeg  600w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-200x150.jpeg  200w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-400x300.jpeg  400w,
                                            https://themes.getbootstrap.com/wp-content/uploads/2019/03/v2.8.2-540x405.jpeg  540w
                                        "
                                        src="./theme_files/v2.8.2-540x405.jpeg"
                                    />
                                </a>
                                <a class="theme-card__body__overlay btn btn-brand btn-sm" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=19799">Live preview</a>
                            </div>
                            <div class="theme-card__footer">
                                <div class="theme-card__footer__item">
                                    <a class="theme-card__title mr-1" href="https://themes.getbootstrap.com/product/falcon-admin-dashboard-webapp-template/">Falcon – Admin Dashboard &amp; WebApp Template</a>
                                    <p class="theme-card__info"></p>
                                    <ul class="prod_cats_list">
                                        <li><a href="https://themes.getbootstrap.com/product-category/admin-dashboard/">Admin &amp; Dashboard</a></li>
                                    </ul>
                                    <p></p>
                                </div>
                                <div class="theme-card__footer__item">
                                    <p class="theme-card__price">
                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>49.00</span>
                                    </p>
                                    <ul class="rating">
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <!-- Removed for now because we're listing all the themes directly on the homepage -->
                <!-- <a class="btn btn-brand btn-block d-md-none mb-4" href="https://themes.getbootstrap.com/shop/?orderby=popularity">View all popular themes</a> -->
            </div>
        </div>
    </section>
@endsection
