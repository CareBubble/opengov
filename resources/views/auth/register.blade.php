@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <main id="main" class="site-main main">
         <section class="section">
            <div class="container">
               <div class="row">
                  <div class="container container--xs">
                     <div class="woocommerce">
                        <div id="signup_div_wrapper">
                           <img class="img-fluid mx-auto d-block mb-3" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/elements/bootstrap-logo.svg" alt="">
                           <h1 class="mb-1 text-center">Sign up</h1>
                           <p class="fs-14 text-gray text-center mb-5">Redownload themes, get support, and review themes.</p>
                           <form method="post" class="register" action="/register">
                             @csrf
                              <p class="woocommerce-FormRow woocommerce-FormRow--first form-row form-row-wide">
                                 <label for="reg_sr_firstname">Name <span class="required">*</span></label>
                                 <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus />
                              </p>
                              <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                 <label for="reg_email">Email address <span class="required">*</span></label>
                                 <input type="email" class="woocommerce-Input woocommerce-Input--text input-text form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" />
                              </p>
                              <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                 <label for="reg_password">Password <span class="required">*</span></label>
                                 <input type="password" class="woocommerce-Input woocommerce-Input--text input-text form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" />
                              </p>
                              <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                 <label for="reg_password">Password <span class="required">*</span></label>
                                 <input type="password" class="woocommerce-Input woocommerce-Input--text input-text"  name="password_confirmation" required autocomplete="new-password" />
                              </p>
                              <!-- Spam Trap -->
                              <div style="left: -999em; position: absolute;"><label for="trap">Anti-spam</label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>
                              <p class="form-row form-row-wide mailchimp-newsletter"><input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="mailchimp_woocommerce_newsletter" type="checkbox" name="mailchimp_woocommerce_newsletter" value="1" checked="checked"> <label for="mailchimp_woocommerce_newsletter" class="woocommerce-form__label woocommerce-form__label-for-checkbox inline"><span>Subscribe for Sales &amp; New Themes</span></label></p>
                              <div class="clear"></div>
                              <div class="woocommerce-privacy-policy-text"></div>
                              <p class="woocomerce-FormRow form-row">
                                 <input type="hidden" id="woocommerce-register-nonce" name="woocommerce-register-nonce" value="f078759c69" /><input type="hidden" name="_wp_http_referer" value="/my-account/" />            <input type="submit" class="btn btn-brand btn-block btn-lg mb-4 mt-3" style="margin:0;" name="register" value="Sign Up" />
                              </p>
                           </form>
                           <p class="text-gray-soft text-center small mb-2">By clicking "Sign up" you agree to our <a href="https://themes.getbootstrap.com/terms">Terms of Service</a>.</p>
                           <p class="text-gray-soft text-center small mb-2">Already have an account? <a href="/login">Sign in</a></p>
                           <!--<p class="text-gray-soft text-center small">Trying to sign up to sell themes? <a href="https://themes.getbootstrap.com/sell/">Apply to be a seller.</a></p>-->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </main>
    </div>
    <!--
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    -->
</div>
@endsection
