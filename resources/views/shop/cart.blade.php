@extends('layouts.app')

@section('content')
    <main id="main" class="site-main main">
       <section class="section">
          <div class="container">
             <div class="row">
                <div class="profile-author mb-4 mb-lg-5 col-12">
                   <div class="profile-author__logo">
                      <a href="https://themes.getbootstrap.com">
                      <img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/elements/bootstrap-logo.svg" />
                      </a>
                   </div>
                   <div class="profile-author__author__description">
                      <a href="https://themes.getbootstrap.com">
                         <h1 class="h2">Bootstrap Themes</h1>
                      </a>
                      <ol class="breadcrumb">
                         <li class="breadcrumb-item"><a href="https://themes.getbootstrap.com">Home</a></li>
                         <li class="breadcrumb-item active">Cart</li>
                      </ol>
                   </div>
                </div>
                <div class="col-lg-5 ml-auto mb-lg-0 mb-3 order-2 mt-lg-4">
                   <div class="ml-0 ml-lg-4 card card--summary">
                      <div class="card-body">
                         <div class="cart_totals calculated_shipping">
                            <table cellspacing="0" class="shop_table shop_table_responsive">
                               <tr class="cart-subtotal">
                                  <th>Subtotal</th>
                                  <td data-title="Subtotal"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>49.00</span></td>
                               </tr>
                               <tr class="order-total">
                                  <th>Total</th>
                                  <td data-title="Total">$49.00</td>
                               </tr>
                            </table>
                            <div class="wc-proceed-to-checkout">
                               <a href="https://themes.getbootstrap.com/checkout/" class="btn btn-lg btn-block btn-brand alt wc-forward">
                               Proceed to checkout</a>
                            </div>
                         </div>
                         <small class="text-center mt-3 d-block"><strong>100% Satisfaction Guarantee</strong><br><span class="text-gray-soft">Don't love your theme? We'll issue you a full refund.</span></small>
                      </div>
                   </div>
                </div>
                <div class="col-lg-7 mt-lg-4 mb-4 mb-lg-0">
                   <!-- Tab panes-->
                   <div class="tab-content">
                      <article id="post-5" class="post-5 page type-page status-publish hentry">
                         <header class="entry-header">
                            <h1 class="entry-title">Cart</h1>
                         </header>
                         <!-- .entry-header -->
                         <div class="entry-content">
                            <div class="woocommerce">
                               <div class="woocommerce-notices-wrapper"></div>
                               <div class="card">
                                  <div class="card-body">
                                     <form class="woocommerce-cart-form" action="https://themes.getbootstrap.com/cart/" method="post">
                                        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents mb-0" cellspacing="0">
                                           <tbody>
                                              <tr class="woocommerce-cart-form__cart-item cart_item">
                                                 <td class="product-thumbnail">
                                                    <div class="card--summary__item">
                                                       <div class="card--summary__theme-thumb">
                                                          <a href="https://themes.getbootstrap.com/product/messenger-responsive-bootstrap-application/"><img width="400" height="300" src="https://themes.getbootstrap.com/wp-content/uploads/2020/01/Messenger-Responsive-Bootstrap-Application2-400x300.jpg" class="attachment-smaller_crop size-smaller_crop" alt="" srcset="https://themes.getbootstrap.com/wp-content/uploads/2020/01/Messenger-Responsive-Bootstrap-Application2-400x300.jpg 400w, https://themes.getbootstrap.com/wp-content/uploads/2020/01/Messenger-Responsive-Bootstrap-Application2-800x600.jpg 800w, https://themes.getbootstrap.com/wp-content/uploads/2020/01/Messenger-Responsive-Bootstrap-Application2.jpg 1200w, https://themes.getbootstrap.com/wp-content/uploads/2020/01/Messenger-Responsive-Bootstrap-Application2-768x576.jpg 768w, https://themes.getbootstrap.com/wp-content/uploads/2020/01/Messenger-Responsive-Bootstrap-Application2-600x450.jpg 600w, https://themes.getbootstrap.com/wp-content/uploads/2020/01/Messenger-Responsive-Bootstrap-Application2-200x150.jpg 200w, https://themes.getbootstrap.com/wp-content/uploads/2020/01/Messenger-Responsive-Bootstrap-Application2-540x405.jpg 540w" sizes="(max-width: 400px) 100vw, 400px" /></a>
                                                       </div>
                                                       <div class="card--summary__theme-description">
                                                          <div>
                                                             <h5 class="card--summary__theme-title">
                                                                <a href="https://themes.getbootstrap.com/product/messenger-responsive-bootstrap-application/">Messenger – Responsive Bootstrap Application</a>
                                                             </h5>
                                                             <dl class="variation">
                                                                <dt class="variation-Vendor">Vendor:</dt>
                                                                <dd class="variation-Vendor">
                                                                   <p>2theme</p>
                                                                </dd>
                                                                <dt class="variation-LicenseType">License Type:</dt>
                                                                <dd class="variation-LicenseType">
                                                                   <p>Standard License</p>
                                                                </dd>
                                                             </dl>
                                                          </div>
                                                       </div>
                                                       <div style="display:none;">
                                                          <div class="quantity">
                                                             <label class="screen-reader-text" for="quantity_60df23e64d490">Messenger – Responsive Bootstrap Application quantity</label>
                                                             <input
                                                                type="number"
                                                                id="quantity_60df23e64d490"
                                                                class="input-text qty text"
                                                                step="1"
                                                                min="0"
                                                                max=""
                                                                name="cart[54abefa5ddc542b4f1b4c650e5325df8][qty]"
                                                                value="1"
                                                                title="Qty"
                                                                size="4"
                                                                inputmode="numeric" />
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </td>
                                                 <td class="product-subtotal" data-title="Total">
                                                    <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>49.00</span>                                <a href="https://themes.getbootstrap.com/cart/?remove_item=54abefa5ddc542b4f1b4c650e5325df8&#038;_wpnonce=f753d57eb3" class="remove" aria-label="Remove this item" data-product_id="38342" data-product_sku="theme_38342">Remove</a>
                                                 </td>
                                              </tr>
                                              <tr>
                                                 <td colspan="6" class="actions pb-0">
                                                    <div class="coupon">
                                                       <label for="coupon_code">Coupon:</label>
                                                       <input type="text" name="coupon_code" class="form-control mr-2" id="coupon_code" value="" placeholder="Coupon code" />
                                                       <input type="submit" class="btn btn-outline-brand" name="apply_coupon" value="Apply" />
                                                    </div>
                                                    <input type="hidden" id="_wpnonce" name="_wpnonce" value="f753d57eb3" /><input type="hidden" name="_wp_http_referer" value="/cart/" />
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                     </form>
                                  </div>
                               </div>
                               <div class="cart-collaterals">
                               </div>
                            </div>
                         </div>
                         <!-- .entry-content -->
                      </article>
                      <!-- #post-5 -->
                   </div>
                </div>
             </div>
          </div>
       </section>
    </main>
@endsection
