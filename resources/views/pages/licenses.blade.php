@extends('layouts.app')

@section('content')
      <section class="section">
         <div class="container">
            <div class="row">
               <div id="primary" class="content-area col-md-8 mx-auto">
                  <div id="content" class="site-content" role="main">
                     <article id="post-112" class="post-112 page type-page status-publish hentry">
                        <header class="entry-header">
                           <h1 class="entry-title">Licenses</h1>
                        </header>
                        <!-- .entry-header -->
                        <div class="entry-content">
                           <h3 id="licenseSummary" class="mt-4 mb-2">Summary</h3>
                           <p>Here’s an overview of what each license allows for to make it easy to pick what you need:</p>
                           <table class="table table-bordered">
                              <tbody>
                                 <tr>
                                    <th>&nbsp;</th>
                                    <th>Standard</th>
                                    <th>Multisite</th>
                                    <th>Extended</th>
                                 </tr>
                                 <tr>
                                    <td>Number of end products</td>
                                    <td>1</td>
                                    <td>Unlimited</td>
                                    <td>1</td>
                                 </tr>
                                 <tr>
                                    <td>Use for personal or a client</td>
                                    <td>✅</td>
                                    <td>✅</td>
                                    <td>✅</td>
                                 </tr>
                                 <tr>
                                    <td>Use in a free end product<br><em>(Can have multiple users)</em></td>
                                    <td>✅</td>
                                    <td>✅</td>
                                    <td>✅</td>
                                 </tr>
                                 <tr>
                                    <td>Use in an end product that is “sold”<br><em>(Can have multiple paying users)</em></td>
                                    <td>❌</td>
                                    <td>❌</td>
                                    <td>✅</td>
                                 </tr>
                                 <tr>
                                    <td>Use in derivative themes or “generators”</td>
                                    <td>❌</td>
                                    <td>❌</td>
                                    <td>❌</td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr>
                           <h3 id="licenseExamples">License Examples</h3>
                           <strong>Examples of Standard License Use: </strong>
                           <ul>
                              <li>A portfolio website for a client</li>
                              <li>A dashboard to visualize your company&#8217;s internal data</li>
                              <li>A marketing site for a paid application (where the Theme and it&#8217;s components are used only on the marketing pages)</li>
                           </ul>
                           <br>
                           <strong>Examples of Multisite License Use:</strong>
                           <ul>
                              <li>All the same examples as the Standard License, but you could build all 3 of them with a single license vs. 3 Standard License copies!</li>
                           </ul>
                           <br>
                           <strong>Examples of Extended License Use:</strong>
                           <ul>
                              <li>A SaaS analytics application</li>
                              <li>A UI library used in a paid app&#8217;s marketing pages, but also pages of the app only available to paying customers</li>
                              <li>A blog with &#8220;paid subscribers only&#8221; areas for their best content</li>
                           </ul>
                           <hr>
                           <h3 id="fullStandardLicense">The Full Standard License</h3>
                           <h5>Basics</h5>
                           <ol>
                              <li>The Standard License grants you a non-exclusive right to make use of Theme you have purchased.</li>
                              <li>You are licensed to use the Item to create one End Product for yourself or for one client (a “single application”), and the End Product can be distributed for Free.</li>
                              <li>
                                 <em>You can…</em>
                                 <ol>
                                    <li>Create one End Product for a client, transfer that End Product to your client, charge them for your services. The license is then transferred to the client.</li>
                                    <li>Make unlimited copies of the single End Product as long as the End Product is distributed for free</li>
                                    <li>Modify the Theme or combine the Theme with other works to make a derivative one. The result is subject to this license. This clause applies to extracting single components from the Theme and using it in derivative works as well.</li>
                                 </ol>
                              </li>
                              <li>
                                 <em>You cannot…</em>
                                 <ol>
                                    <li>Sell the End Product except to one client.</li>
                                    <li>Re-distribute the Theme’s source, modified or not, even if the Theme is modified.</li>
                                    <li>Use the Theme in any application allowing an End User to customize the Theme, like “build it yourself” or “theme generators”.</li>
                                    <li>Allow the End User of the End Product to access the Theme’s source and use it separately from the End Product.</li>
                                 </ol>
                              </li>
                              <li>If a Theme contains code, images, or content sourced from elsewhere under a different license, that item retains its original license. The license for the code, images, or content will be noted by the Theme author. You are responsible for adhering to the original license or clearing it with the author of the code, image, or content.</li>
                              <li>This license can be terminated if you breach it and you lose the right to distribute the End Product until the Theme has been fully removed from the End Product.</li>
                              <li>The author of the Theme retains ownership of the Theme, but grants you the license on these terms. This license is between the author of the Theme and you. Be Colossal LLC (Bootstrap Themes) are not a party to this license or the one granting you the license.</li>
                           </ol>
                           <hr>
                           <h3 id="fullMultisiteLicense">The Full Multisite License</h3>
                           <h5>Basics</h5>
                           <ol>
                              <li>The Multisite License grants you a non-exclusive right to make use of Theme you have purchased.</li>
                              <li>You are licensed to use the Item to create unlimited End Products for yourself or multiple clients, and the End Products can be distributed for Free.</li>
                              <li>
                                 <em>You can…</em>
                                 <ol>
                                    <li>Create unlimited End Products for a client, transfer that End Product to your client, charge them for your services. The equivalent of a Standard License is granted to the client. They do not assume the unlimited use nature of the Multisite License.</li>
                                    <li>Make unlimited copies of the single End Product as long as the End Product is distributed for free</li>
                                    <li>Modify the Theme or combine the Theme with other works to make a derivative one. The result is subject to this license. This clause applies to extracting single components from the Theme and using it in derivative works as well.</li>
                                 </ol>
                              </li>
                              <li>
                                 <em>You cannot…</em>
                                 <ol>
                                    <li>Sell the End Product except to clients.</li>
                                    <li>Re-distribute the Theme’s source, modified or not, even if the Theme is modified.</li>
                                    <li>Use the Theme in any application allowing an End User to customize the Theme, like “build it yourself” or “theme generators”. </li>
                                    <li>Allow the End User of the End Product to access the Theme’s source and use it separately from the End Product.</li>
                                 </ol>
                              </li>
                              <li>If a Theme contains code, images, or content sourced from elsewhere under a different license, that item retains its original license. The license for the code, images, or content will be noted by the Theme author. You are responsible for adhering to the original license or clearing it with the author of the code, image, or content.</li>
                              <li>This license can be terminated if you breach it and you lose the right to distribute the End Products until the Theme has been fully removed from the End Products.</li>
                              <li>The author of the Theme retains ownership of the Theme, but grants you the license on these terms. This license is between the author of the Theme and you. Be Colossal LLC (Bootstrap Themes) are not a party to this license or the one granting you the license.</li>
                           </ol>
                           <hr>
                           <h3 id="fullExtendedLicense">The Full Extended License</h3>
                           <h5>Basics</h5>
                           <ol>
                              <li>The Extended License grants you a non-exclusive right to make use of Theme you have purchased.</li>
                              <li>You are licensed to use the Item to create one End Product for yourself or for one client (a “single application”), and the End Product maybe sold or distributed for free.</li>
                              <li>
                                 <em>You can…</em>
                                 <ol>
                                    <li>Create one End Product for a client, transfer that End Product to your client, charge them for your services. The license is then transferred to the client.</li>
                                    <li>Sell and make any number of copies of the single End Product.</li>
                                    <li>Modify the Theme or combine the Theme with other works to make a derivative one. The result is subject to this license. This clause applies to extracting single components from the Theme and using it in derivative works as well.</li>
                                 </ol>
                              </li>
                              <li>
                                 <em>You cannot…</em>
                                 <ol>
                                    <li>Use the Theme to create more than one unique End Product. This license is a “single application” license and not a “multi-use” license.</li>
                                    <li>Re-distribute the Theme’s source, modified or not, even if the Theme is modified.</li>
                                    <li>Use the Theme in any application allowing an End User to customize the Theme, like “build it yourself” or “theme generators”. You can purchase separate licenses for each final product incorporating the Theme that is created using the application.</li>
                                    <li>Allow the End User of the End Product to access the Theme’s source and use it separately from the End Product.</li>
                                 </ol>
                              </li>
                              <li>If a Theme contains code, images, or content sourced from elsewhere under a different license, that item retains its original license. The license for the code, images, or content will be noted by the Theme author. You are responsible for adhering to the original license or clearing it with the author of the code, image, or content.</li>
                              <li>This license can be terminated if you breach it and you lose the right to distribute the End Product until the Theme has been fully removed from the End Product.</li>
                              <li>The author of the Theme retains ownership of the Theme, but grants you the license on these terms. This license is between the author of the Theme and you. Be Colossal LLC (Bootstrap Themes) are not a party to this license or the one granting you the license.</li>
                           </ol>
                           <hr>
                           <h5 class="mt-3">Helpful examples:</h5>
                           <ul>
                              <li>Will list some helpful examples of what is acceptable use soon!</li>
                           </ul>
                           <hr>
                           <h5 class="mt-3">Definitions:</h5>
                           <ul>
                              <li>Theme is purchased digital work</li>
                              <li>End Product is a customized implementation of the Theme</li>
                              <li>End User is a user of the End Product</li>
                              <li>Client is a contracted employer of the license holder</li>
                           </ul>
                           <hr>
                           <h5 class="mt-3">Need help picking your license?</h5>
                           <p>Just shoot us an email at <a href="mailto:themes@getbootstrap.com">themes@getbootstrap.com</a> and we&#8217;ll help you decide which license makes sense for your needs!</p>
                           <hr>
                           <h5 class="mt-3">Want to upgrade your license?</h5>
                           <p>Our article about <a href="https://themes.zendesk.com/hc/en-us/articles/360039416971-Upgrade-License-Type">upgrading your license</a> covers the process, but it&#8217;s basically a quick email with a few details!</p>
                        </div>
                        <!-- .entry-content -->
                     </article>
                     <!-- #post-112 -->
                  </div>
                  <!-- #content .site-content -->
               </div>
               <!-- #primary .content-area -->
            </div>
         </div>
      </section>
@endsection
