@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <section class="hero hero--xs w-100 pt-0">
                    <div class="container">
                        <h1 class="display-2 text-bold mb-3">Software built for emerging nations.</h1>
                        <h5 class="text-gray-soft text-regular">open source apps that let you connect to your constituents and allow them to have their say in a useable and secure way.</h5>
                    </div>
                </section>
                <section class="section pt-3 official-themes-page pb-0">
                    <div class="screenshots-masonry"> <img class="d-none d-lg-block" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/theme-masonry.jpg"> <img class="d-block d-lg-none" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/theme-masonry-mobile.jpg"></div>
                    <div
                        class="container">
                        <div class="row align-items-center justify-content-center mb-1 mt-5 mb-md-5 text-center">
                            <div class="col-md-3 col-6 mb-5 mb-md-0 border-right">
                                <h6 class="mb-1">Average Rating</h3>
                                    <div class="d-flex align-items-center justify-content-center">
                                        <ul class="rating d-inline mb-0 mr-2">
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                        </ul>
                                        <p class="fs-13 text-gray-soft d-inline">4.9/5</p>
                                    </div>
                            </div>
                            <div class="col-md-3 col-6 mb-5 mb-md-0 border-right">
                                <h6 class="mb-1">Our Guarantee</h3>
                                    <p class="fs-13 text-gray-soft">100% money back.</p>
                            </div>
                            <div class="col-md-3 col-6 mb-5 mb-md-0 border-right">
                                <h6 class="mb-1">Free Updates</h3>
                                    <p class="fs-13 text-gray-soft">Never pay for an update.</p>
                            </div>
                            <div class="col-md-3 col-6 mb-5 mb-md-0">
                                <h6 class="mb-1">Free Support</h3>
                                    <p class="fs-13 text-gray-soft">6 months included</p>
                            </div>
                        </div>
                        <div class="row align-items-center justify-content-between py-4 ">
                            <div class="col-xl-5 col-lg-6 pt-4">
                                <h1 class="mb-2 text-bold w-75">Built as frameworks from the ground up.</h3>
                                    <p class="fs-16 text-gray-soft">
                                          Our GovTech is designed to be modular and all our apps communicate with each other building a robust and interconnected IT infratructure.
                                    </p>
                            </div>
                            <div class="col-lg-12 mb-5"> <img class="img-fluid mx-auto framework-progression-graphic d-none d-lg-block" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/framework-progression.png" alt="">                                    <img class="img-fluid mx-auto framework-progression-graphic d-block d-lg-none" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/framework-progression-mobile.jpg"
                                    alt=""></div>
                        </div>
                        <div class="row align-items-center justify-content-between">
                            <div class="col-lg-6 mt-5 order-2 order-lg-1"><img class="img-fluid mx-auto d-block" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/review-process.svg" alt=""></div>
                            <div class="col-lg-5 order-1 order-lg-2">
                                <h1 class="mb-2 text-bold w-75">Fast, Fair and Secure</h3>
                                    <p class="fs-16 text-gray-soft">
                                        we build the most secure blockchain based  GovTech to enable small and emerging countries who have little-to-no technology infrastructue to quickly implimet IT infrastuctue such as populaiton managmement, births, death & marriages, voting systems and law managmement stytems without spending a fortune on resources.
                                    </p>
                            </div>
                        </div>
                        <div class="row align-items-center justify-content-between mt-5 my-lg-5 pt-5">
                            <div class="col-lg-5 mb-3 mb-lg-5">
                                <h1 class="mb-2 text-bold w-75">Build tools and full documention.</h3>
                                    <p class="fs-16 text-gray-soft">
                                        We offer APIs, SDKs and iOS framworks so if you have your own IT staff they can quickly get to grips with our apps and how they work using our handy <a href="/guide">guide</a>.
                                    </p> <a class="btn btn-brand" href="/guide">Explore the Dashboard docs</a></div>
                            <div class="col-lg-6 mb-4 mt-4"><img class="img-fluid mx-auto d-block" src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/terminal.jpg" alt=""></div>
                        </div>
                        <div class="row align-items-center justify-content-between mb-5 pt-5 mb-lg-3 text-center">
                            <div class="col-sm-4 px-sm-4 pb-5"> <img src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/components-icon.svg">
                                <h5 class="my-2 text-bold">Components and examples</h5>
                                <p class="text-gray-soft">Each theme features new components built to match Bootstrap's level of quality and re-usability.</p>
                            </div>
                            <div class="col-sm-4 px-sm-4 pb-5"> <img src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/sliders-icon.svg">
                                <h5 class="my-2 text-bold">Tons of variables</h5>
                                <p class="text-gray-soft">Themes inherit their style from custom variables, like Bootstrap, so basic customization is easy.</p>
                            </div>
                            <div class="col-sm-4 px-sm-4 pb-5"> <img src="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/official-themes/wrenches-icon.svg">
                                <h5 class="my-2 text-bold">Source files</h5>
                                <p class="text-gray-soft">Each theme includes all the source and compiled files, making deep customization possible.</p>
                            </div>
                        </div>
            </div>
            </section>
        </div>
        </div>
    </section>
@endsection
