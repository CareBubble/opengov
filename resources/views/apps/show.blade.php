@extends('layouts.app')

@section('content')

        <section class="section">
            <div class="container">
                <div class="row">
                    <div class='col'>
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main" role="main">
                                <div class="woocommerce-notices-wrapper"></div>
                                <div id="title">&nbsp;</div>
                                <div class="row">
                                    <div class="col-lg-8 mb-md-0 mb-3">
                                        <div id="product-17532" class="post-17532 product type-product status-publish has-post-thumbnail product_cat-ecommerce-retail first instock downloadable shipping-taxable purchasable product-type-simple">
                                            <div class="feature-screenshot">
                                                <div class="" style="opacity: 1; transition: opacity .25s ease-in-out;">
                                                    <div id="mainimage" class=""></div>
                                                </div><a class="feature-screenshot__overlay" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=17532"> <button class="btn btn-inverted">Launch live preview</button> </a> </div>
                                            <div class="d-lg-none">
                                                <div class="d-flex justify-content-between align-items-center mb-3">
                                                    <div>
                                                        <h4 class="mt-3 mb-1">Directory – Directory &#038; Listing Bootstrap 5 Theme</h4>
                                                        <div class="dropdown"> <a class="dropdown-toggle link--dark" js-price-dropdown="true" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Standard License</a>
                                                            <div class="dropdown-menu dropdown-menu--xl dropdown-menu--centered dropdown-menu--has-triangle">
                                                                <button class="dropdown-block-item switch_price_prod" data-type="Standard License" data-price="0.00" data-price_label="$0.00" data-label="Standard License"> <div class="d-flex justify-content-between align-items-center mb-2"><span>Standard GNU 2.0 License</span><span class="d-flex align-items-center"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>0.00</span></span></div><ul class="fs-13 text-gray-soft mb-2"> <li>Use for a single product</li><li>Non-paying users only</li></ul> <p class="fs-11 text-gray-soft">Read the full <a href="https://themes.getbootstrap.com/licenses#fullStandardLicense">Standard License</a></p></button>
                                                                <div class="dropdown-divider"></div><button class="dropdown-block-item switch_price_prod" data-type="Multisite License" data-price="149.00" data-price_label="$149.00" data-label="Multisite License"> <div class="d-flex justify-content-between align-items-center mb-2"><span>Multisite License</span><span class="d-flex align-items-center">$149.00</span></div><ul class="fs-13 text-gray-soft mb-2"> <li>Use for a unlimited product</li><li>Non-paying users only</li></ul> <p class="fs-11 text-gray-soft">Read the full <a href="https://themes.getbootstrap.com/licenses#fullMultisiteLicense">Multisite License</a></p></button>
                                                                <div class="dropdown-divider"></div><button class="dropdown-block-item switch_price_prod" data-type="Extended License" data-price="590.00" data-price_label="$590.00" data-label="Extended License"> <div class="d-flex justify-content-between align-items-center mb-2"><span>Extended License</span><span class="d-flex align-items-center">$590.00</span></div><ul class="fs-13 text-gray-soft mb-2"> <li>Use for a single product</li><li>Paying users allowed</li></ul> <p class="fs-11 text-gray-soft">Read the full <a href="https://themes.getbootstrap.com/licenses#fullExtendedLicense">Extended License</a></p></button>                                                                </div>
                                                        </div>
                                                    </div>
                                                    <h3 class="d-flex align-items-center" js-price-value="main_price_div"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>49.00</span>
                                                    </h3>
                                                </div>
                                                <div class="d-flex justify-content-between align-items-center mb-3"> <a class="btn btn-brand btn-block" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=17532">Live preview</a>
                                                    <form action="https://themes.getbootstrap.com/cart/" method="POST" class="d-block w-100">
                                                      <input type="hidden" js-license-type="license_type" name="license_type" value="Standard License" /> <input type="hidden" name="add-to-cart" value="17532" />
                                                    <button type="submit" class="btn btn-brand btn-block btn-checkout mt-0 ml-1">
                                                       <span class="btn-text">Add to cart</span></a> </form> </div>
                                                       <div class="theme-purchases">
                                                          <div class="theme-purchases__item">
                                                             <a class="theme-purchases__item__inner text-center" data-toggle="tab" href="#reviews-tab" role="tab" js-handle="review-toggler">
                                                                <ul class="rating justify-content-center">
                                                                   <li class="rating__item rating__item--active"></li>
                                                                   <li class="rating__item rating__item--active"></li>
                                                                   <li class="rating__item rating__item--active"></li>
                                                                   <li class="rating__item rating__item--active"></li>
                                                                   <li class="rating__item rating__item--active"></li>
                                                                </ul>
                                                                <p>5.00/5 (18 reviews)</p>
                                                             </a>
                                                             <div class="theme-purchases__item__inner text-center">
                                                                <h5 class="mb-0"><i class="bootstrap-themes-icon-cart"></i>973</h5>
                                                                <p>Purchases</p>
                                                             </div>
                                                          </div>
                                                       </div>
                                                       </div>
                                                       <div class="d-flex justify-content-between align-items-center has-border">
                                                          <ul class="nav sub-nav sub-nav--has-border" role="tablist">
                                                             <li class="nav-item"><a class="nav-link sub-nav-link active" data-toggle="tab" href="#description-tab" role="tab">Description</a></li>
                                                             <li class="nav-item"><a class="nav-link sub-nav-link" data-toggle="tab" href="#reviews-tab" role="tab">Reviews</a></li>
                                                             <li class="nav-item"><a class="nav-link sub-nav-link" data-toggle="tab" href="#changelog-tab" role="tab">Changelog</a></li>
                                                          </ul>
                                                          <ul class="d-none list-social justify-content-end">
                                                             <li class="list-social__item">Share:</li>
                                                             <li class="list-social__item"><a class="bootstrap-themes-icon-facebook-squared list-social__link" target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fthemes.getbootstrap.com%2Fproduct%2Fdirectory-directory-listing-bootstrap-4-theme%2F&title=Directory+%E2%80%93+Directory+%26%23038%3B+Listing+Bootstrap+5+Theme"></a></li>
                                                             <li class="list-social__item"><a class="bootstrap-themes-icon-pinterest-squared list-social__link" target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media=https%3A%2F%2Fthemes.getbootstrap.com%2Fwp-content%2Fuploads%2F2019%2F02%2Fdirectory-b5-1200x900.jpg&url=https%3A%2F%2Fthemes.getbootstrap.com%2Fproduct%2Fdirectory-directory-listing-bootstrap-4-theme%2F&is_video=false&description=Directory+%E2%80%93+Directory+%26%23038%3B+Listing+Bootstrap+5+Theme"></a></li>
                                                             <li class="list-social__item"><a class="bootstrap-themes-icon-twitter list-social__link" target="_blank" href="http://twitter.com/intent/tweet?status=Directory+%E2%80%93+Directory+%26%23038%3B+Listing+Bootstrap+5+Theme+https%3A%2F%2Fthemes.getbootstrap.com%2Fproduct%2Fdirectory-directory-listing-bootstrap-4-theme%2F"></a></li>
                                                          </ul>
                                                       </div>
                                                       <div class="tab-content">
                                                       <div class="tab-pane fade show mt-2 mt-lg-5 active" id="description-tab" role="tabpanel">
                                                          <div class="theme-description">
                                                              <div id="descriptionText">
                                                                  &nbsp;
                                                              </div>
                                                          </div>
                                                       </div>
                                                       <div class="tab-pane fade" id="reviews-tab" role="tabpanel">
                                                       <div class="theme-review__submission-prompt d-flex justify-content-between align-items-center">
                                                          <div id="start_review_msg" class="">
                                                             <h6 class="mb-0">You must purchase this theme to leave a review.</h6>
                                                             <p class="text-gray fs-14">If you have already purchased it, <a href="https://themes.getbootstrap.com/signin/">login</a> to leave a review.</p>
                                                          </div>
                                                       </div>
                                                       <div id="review_form_response1" style="display:none;" class="alert"></div>
                                                       <div class="theme-review">
                                                       <form action="" id="review_submit_form">
                                                       <input type="hidden" name="action" value="review_submit"/> <input type="hidden" name="post_id" value="17532"/>
                                                       <div id="review_submit_form_overlay" > <i class="fa fa-spinner fa-spin"></i> </div>
                                                       <div class="row">
                                                       <div class="form-group col-md-8 mb-3">
                                                          <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle">
                                                          <div class="invalid-feedback">Please provide a title</div>
                                                       </div>
                                                       <div class="form-group col-md-4 mb-3">
                                                          <label class="col-form-label" for="reviewScore">Review</label>
                                                          <select class="form-control required" name="reviewScore" id="reviewScore">
                                                             <option value="5">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option>
                                                             <option value="4">&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option>
                                                             <option value="3">&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option>
                                                             <option value="2">&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option>
                                                             <option value="1">&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option>
                                                          </select>
                                                          <div class="invalid-feedback">Please select a score</div>
                                                       </div>
                                                       <div class="form-group col-12">
                                                          <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_count">0/500</span>
                                                          <textarea class="form-control required" name="reviewBody" id="reviewBody"></textarea>
                                                          <div class="invalid-feedback">Please enter a review</div>
                                                       </div>
                                                       <div class="form-group col-12">
                                                    <button class="btn btn-brand btn-block" id="post_review" type="button">Post review</button>
                                                  </div>
                                            </div>
                                            </form>
                                        </div>
                                        <div id="review_list" class="mt-4">
                                            <div class="theme-review" id="comment_171613">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Broken dropdown</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by sungchan</li>
                                                                <li class="list-dotted__item">2 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="171613" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Broken dropdown" />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">I download today! but header's dropdown icon does not shown! please fix this :) ----You are right. It's my config problem!! Thank you. And would you please add component for autocomplete? like Airbnb's place search! Is it already possible?</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>I download today! but header's dropdown icon does not shown! please fix this :) <br/>----<br/>You are right. It's my config problem!! Thank you. <br/>And would you please add component for autocomplete?
                                                                like Airbnb's place search! Is it already possible?</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="171616" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Hey Sungchan, Thank you for your review, I'm glad that you like the theme! I'll get in touch re. your problem with the dropdown. It looks fine on my end, so it might be e.g. an issue with your config.Thank you for your purchase 🙂Ondrej, Bootstrapious</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Hey Sungchan, <br/><br/>Thank you for your review, I'm glad that you like the theme! <br/><br/>I'll get in touch re. your problem with the dropdown. It looks fine on my end, so it might
                                                                        be e.g. an issue with your config.<br/><br/>Thank you for your purchase 🙂<br/><br/>Ondrej, Bootstrapious</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_149280">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Great Theme</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Ellis</li>
                                                                <li class="list-dotted__item">5 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="149280" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Great Theme" />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">Designer did a great job, thank you for your work. Modern design, loads of stuff to work with. Always easy to come up with a new page by playing around with existing template pages if you need a new page design. Only bit of constructive feedback I'd give is to add an about us page. I used the blog post page and removed the author details and it looked great as an about us.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>Designer did a great job, thank you for your work. Modern design, loads of stuff to work with. Always easy to come up with a new page by playing around with existing template pages if you need
                                                                a new page design. Only bit of constructive feedback I'd give is to add an about us page. I used the blog post page and removed the author details and it looked great as an about us.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="171617" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you very much for your review, Ellis.I'm working on an upgrade to Bootstrap 5 now, but you're completely right about the about page. I'll definitely add this soon.Have a great day,Ondrej</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you very much for your review, Ellis.<br/><br/>I'm working on an upgrade to Bootstrap 5 now, but you're completely right about the about page. I'll definitely add this soon.<br/><br/>Have
                                                                        a great day,<br/><br/>Ondrej</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_148538">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Excellent for search-centered web-apps</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Garrett</li>
                                                                <li class="list-dotted__item">5 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="148538" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Excellent for search-centered web-apps"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">This template has all of the designs, styles, and components needed to put together an incredible search-centered web application. I use it in conjunction with a different Bootstrap Admin theme for admin panel access, and I have not encountered any issues. I look forward to using this template to learn about SASS and CSS precompiling to further speed up front-end development.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>This template has all of the designs, styles, and components needed to put together an incredible search-centered web application. I use it in conjunction with a different Bootstrap Admin theme
                                                                for admin panel access, and I have not encountered any issues. <br/><br/>I look forward to using this template to learn about SASS and CSS precompiling to further speed up front-end development.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="171619" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you, Garrett!</textarea> <button class="btn btn-brand edit_reply"
                                                                                type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you, Garrett!</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_144692">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Well documented template with great support.</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Tim</li>
                                                                <li class="list-dotted__item">6 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="144692" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Well documented template with great support."
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">Great template. The process of using a template was new for me and the documentation was of great help. I used it with a Django project and Ondrej gave me some good guidance. Thereafter, he promptly replied to all my messages. Thanks Ondrej!</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>Great template. The process of using a template was new for me and the documentation was of great help. I used it with a Django project and Ondrej gave me some good guidance. Thereafter, he promptly
                                                                replied to all my messages. Thanks Ondrej!</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="171618" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you for your kind words, Tim ✌✌</textarea> <button class="btn btn-brand edit_reply"
                                                                                type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you for your kind words, Tim ✌✌</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_143361">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Recommended.</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Ivan</li>
                                                                <li class="list-dotted__item">7 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="143361" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Recommended." />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">The template is very good. I needed assistance and they got back to me right away. Recommended.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>The template is very good. I needed assistance and they got back to me right away. Recommended.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">7 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="143404" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you very much for your kind words, Ivan 🙏🙏All the best with your project!</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you very much for your kind words, Ivan 🙏🙏<br/><br/>All the best with your project!</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_142603">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>The theme is great, the Bootstrapious team even better!</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Carlos</li>
                                                                <li class="list-dotted__item">7 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="142603" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="The theme is great, the Bootstrapious team even better!"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">The code quality of this theme is exceptional.But more importantly it has a team that stand strong behind it, wether it is support or custom work, Ondrej and his team at Bootstrapious are there for you. Cannot recommend the theme and them enough. </textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>The code quality of this theme is exceptional.<br/>But more importantly it has a team that stand strong behind it, wether it is support or custom work, Ondrej and his team at Bootstrapious are
                                                                there for you. <br/>Cannot recommend the theme and them enough. </p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">7 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="143101" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Carlos, Thank you for the stellar review 😊 It has been a pleasure working with you.</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Carlos, Thank you for the stellar review 😊 It has been a pleasure working with you.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_141786">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Excellent Theme</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Ryan</li>
                                                                <li class="list-dotted__item">7 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="141786" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Excellent Theme" />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">An excellent, professionally-designed theme.Very flexible, well-balanced design elements and consistent layouts.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>An excellent, professionally-designed theme.<br/>Very flexible, well-balanced design elements and consistent layouts.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">7 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="143102" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you very much for your kind words, Ryan. Glad you liked the theme!Kind regards,Ondrej</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you very much for your kind words, Ryan. Glad you liked the theme!<br/><br/>Kind regards,<br/><br/>Ondrej</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_134888">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Perfect theme for an "airbnb for X"</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Benoit</li>
                                                                <li class="list-dotted__item">9 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="134888" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Perfect theme for an "
                                                                                airbnb for X ""/>
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">5 stars ***** ! A very good template that really speed up my front-end development. Easy to install and tweak, very good examples, bootstrap classes are still useable. Thanks a lot Bootstrapious !Side note: I would love to see in a near future an admin template with same components combined with charts as well as calendar pages for hosts=)All the best !</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>5 stars ***** ! A very good template that really speed up my front-end development. Easy to install and tweak, very good examples, bootstrap classes are still useable. Thanks a lot Bootstrapious
                                                                !<br/><br/>Side note: I would love to see in a near future an admin template with same components combined with charts as well as calendar pages for hosts=)<br/><br/>All the best !</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">7 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="143098" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Hi Benoit, Thank you very much for your positive review.Note taken. I will try to release some charts in the next update 🤞All the best!Ondrej, Bootstrapious</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Hi Benoit, <br/><br/>Thank you very much for your positive review.<br/><br/>Note taken. I will try to release some charts in the next update 🤞<br/><br/>All the best!<br/><br/>Ondrej,
                                                                        Bootstrapious</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_122050">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Fantastic professional template</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Ganesh</li>
                                                                <li class="list-dotted__item">12 months ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="122050" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Fantastic professional template"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">This is one of the best template out there for directory theme. I am building my site around it and it's great.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>This is one of the best template out there for directory theme. I am building my site around it and it's great.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">10 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="130558" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you, Ganesh 😇</textarea> <button class="btn btn-brand edit_reply"
                                                                                type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you, Ganesh 😇</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_114820">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Exactly what we needed</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by M</li>
                                                                <li class="list-dotted__item">1 year ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="114820" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Exactly what we needed"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">Best directory theme out there, love it. No problems whatsoever. Thanks Bootstrapious!</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>Best directory theme out there, love it. No problems whatsoever. Thanks Bootstrapious!</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">10 months ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="130559" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Hey, Thank you so much for your positive review. I'm glad that you enjoyed working with the theme.Cheers,Ondrej, Bootstrapious</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Hey, <br/><br/>Thank you so much for your positive review. I'm glad that you enjoyed working with the theme.<br/><br/>Cheers,<br/><br/>Ondrej, Bootstrapious</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_107760">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Awesome</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Aurelien</li>
                                                                <li class="list-dotted__item">1 year ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="107760" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Awesome" />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">This "Bootstrap Themes" is Awesome. All the template pages provided are great and easy to use for customizing my project. Moreover, the code is clean, that is an advantage to pick some components and create new pages.The support service is excellent!I highly recommend this theme and the team Boostrapious.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>This "Bootstrap Themes" is Awesome. All the template pages provided are great and easy to use for customizing my project. Moreover, the code is clean, that is an advantage to pick some components
                                                                and create new pages.<br/>The support service is excellent!<br/>I highly recommend this theme and the team Boostrapious.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">1 year ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="107822" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you very much for your kind words, Aurelien. I'm very happy that you're satisfied 🙏🙏Cheers,Ondrej, Bootstrapious</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you very much for your kind words, Aurelien. I'm very happy that you're satisfied 🙏🙏<br/><br/>Cheers,<br/><br/>Ondrej, Bootstrapious</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_99916">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Great theme, fantastic support </h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Thierry</li>
                                                                <li class="list-dotted__item">1 year ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="99916" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Great theme, fantastic support "
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">Easy to implement thanks to the code readability. Enough templates to address most of our use case. The support Ondrej provides is fast, straight to the point, and solved our issues. Very happy with choosing bootstrapious. </textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>Easy to implement thanks to the code readability. Enough templates to address most of our use case. The support Ondrej provides is fast, straight to the point, and solved our issues. Very happy
                                                                with choosing bootstrapious. </p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">1 year ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="99924" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thanks a lot, Thierry 🙂🙂 I am so glad that the theme serves you well.Have a great day!Ondrej</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thanks a lot, Thierry 🙂🙂 I am so glad that the theme serves you well.<br/><br/>Have a great day!<br/><br/>Ondrej</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_99638">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Beautiful theme with well fleshed out examples</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Andrew</li>
                                                                <li class="list-dotted__item">1 year ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="99638" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Beautiful theme with well fleshed out examples"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">Excellent theme that is super easy to integrate with and looks amazing. Developer is responsive and the theme is still being updated, even 12 months after release.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>Excellent theme that is super easy to integrate with and looks amazing. Developer is responsive and the theme is still being updated, even 12 months after release.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">1 year ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="99923" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you for your great review, Andrew. I am happy that you're satisfied ✌✌Cheers,Ondrej, Bootstrapious</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you for your great review, Andrew. I am happy that you're satisfied ✌✌<br/><br/>Cheers,<br/><br/>Ondrej, Bootstrapious</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_81404">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Love this theme!</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Scott</li>
                                                                <li class="list-dotted__item">2 years ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="81404" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Love this theme!"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">Was working on a project for a client that is in this exact industry and this theme fit our needs perfectly. I had a couple of questions about modifying the theme. The developer was very helpful and quick to respond. Highly recommend.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>Was working on a project for a client that is in this exact industry and this theme fit our needs perfectly. I had a couple of questions about modifying the theme. The developer was very helpful
                                                                and quick to respond. Highly recommend.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 years ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="83999" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you for your review, Scott. I am happy that the theme serves well 🎉Cheers, Ondrej, Bootstrapious</textarea>                                                                            <button class="btn btn-brand edit_reply" type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you for your review, Scott. I am happy that the theme serves well 🎉<br/><br/>Cheers, <br/><br/>Ondrej, Bootstrapious</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_53839">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Well built, beautiful and flexible theme</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Neville</li>
                                                                <li class="list-dotted__item">2 years ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="53839" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Well built, beautiful and flexible theme"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">Very happy with this theme. The components are well documented and the layers all work well with each other horizontally which means that the theme is very customizable.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>Very happy with this theme. The components are well documented and the layers all work well with each other horizontally which means that the theme is very customizable.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 years ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="84012" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thanks lot for your review, Neville! Cheers,Ondrej 😊</textarea>
                                                                            <button
                                                                                class="btn btn-brand edit_reply" type="button">Update reply</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thanks lot for your review, Neville! <br/><br/>Cheers,<br/><br/>Ondrej 😊</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_36480">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>Great theme & very versatile</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Chris</li>
                                                                <li class="list-dotted__item">2 years ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="36480" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="Great theme & very versatile"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">I am really pleased with this theme - very easy to work with and to adapt to my needs - exactly what I was looking for!</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>I am really pleased with this theme - very easy to work with and to adapt to my needs - exactly what I was looking for!</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 years ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="84000" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you, Chris ✌</textarea> <button class="btn btn-brand edit_reply"
                                                                                type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you, Chris ✌</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_35273">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>DESIGN & CODE QUALITY</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by Morgan</li>
                                                                <li class="list-dotted__item">2 years ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="35273" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="DESIGN & CODE QUALITY"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">GREAT THEME! Theme is just amazing, clean coded, light and nicely designed. It fits all the requirements for my new project. I would say, for me, definitely one of the best b4.x currently available on the market. Keep up the good work and thanks again for the the great support you provide Ondrej! Good luck with sales!</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>GREAT THEME! Theme is just amazing, clean coded, light and nicely designed. It fits all the requirements for my new project. I would say, for me, definitely one of the best b4.x currently available
                                                                on the market. Keep up the good work and thanks again for the the great support you provide Ondrej! Good luck with sales!</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 years ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="84013" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you, for your nice words, Morgan. 🙏</textarea> <button class="btn btn-brand edit_reply"
                                                                                type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you, for your nice words, Morgan. 🙏</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="theme-review" id="comment_34608">
                                                <div class="theme_review_item">
                                                    <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                    <div class="theme-review__heading">
                                                        <div class="theme-review__heading__item">
                                                            <h6>very much recommended</h6>
                                                            <ul class="list-dotted">
                                                                <li class="list-dotted__item">by francisc</li>
                                                                <li class="list-dotted__item">2 years ago</li>
                                                            </ul>
                                                        </div>
                                                        <div class="theme-review__heading__item">
                                                            <ul class="rating">
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                                <li class="rating__item rating__item--active"></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="theme-review__body">
                                                        <div class="main_reply_form">
                                                            <div class="media-body">
                                                                <form action="" class="review_edit_form"> <input type="hidden" name="action" value="review_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id" value="34608" />
                                                                    <div class="row">
                                                                        <div class="form-group col-md-8 mb-3"> <label class="col-form-label" for="reviewTitle">Review title</label> <input class="form-control required" type="text" name="reviewTitle" id="reviewTitle" value="very much recommended"
                                                                            />
                                                                            <div class="invalid-feedback">Please provide a title</div>
                                                                        </div>
                                                                        <div class="form-group col-md-4 mb-3"> <label class="col-form-label" for="reviewScore">Review</label> <select class="form-control required" name="reviewScore" id="reviewScore"> <option value="5" selected="selected">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option> <option value="4" >&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option> <option value="3" >&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option> <option value="2" >&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option> <option value="1" >&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option> </select>
                                                                            <div class="invalid-feedback">Please select a score</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <label class="col-form-label" for="reviewBody">Review</label> <span class="form-sublink" id="reviewBody_edit_count">0/500</span> <textarea class="form-control required" name="reviewBody"
                                                                                id="reviewBody_edit">the quality of the code is very good, also very prompt support. The theme is also very versatile, you can adapt it to your needs easily.</textarea>
                                                                            <div class="invalid-feedback">Please enter a review</div>
                                                                        </div>
                                                                        <div class="form-group col-12"> <button class="btn btn-brand btn-block review_edit_button" type="button">Update review</button> </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="main_reply_body">
                                                            <p>the quality of the code is very good, also very prompt support. The theme is also very versatile, you can adapt it to your needs easily.</p>
                                                        </div>
                                                    </div>
                                                    <div class="theme_review_item">
                                                        <div class="review_submit_form_overlay"> <i class="fa fa-spinner fa-spin"></i> </div>
                                                        <div class="theme-review__reply media">
                                                            <div class="profile-author__logo d-flex mr-3"><img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt="" /></div>
                                                            <div class="media-body">
                                                                <div class="theme-review__reply__heading mt-0 d-block">Bootstrapious</div>
                                                                <ul class="list-dotted mb-2">
                                                                    <li class="list-dotted__item">Theme Creator</li>
                                                                    <li class="list-dotted__item">2 years ago</li>
                                                                </ul>
                                                                <div class="comm_reply_form">
                                                                    <div class="media-body">
                                                                        <form action="" class="reply_box"> <span class="form-sublink">0/500</span> <input type="hidden" name="action" value="review_reply_edit" /> <input type="hidden" name="post_id" value="17532" /> <input type="hidden" name="comm_id"
                                                                                value="84011" /> <textarea name="reply_body" class="reply_body theme-review__reply_input form-control mb-2">Thank you, Francisc! 🙏</textarea> <button class="btn btn-brand edit_reply"
                                                                                type="button">Update reply</button> </form>
                                                                    </div>
                                                                </div>
                                                                <div class="comm_reply_body">
                                                                    <p class="theme-review__reply__body">Thank you, Francisc! 🙏</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade mt-4 mt-lg-5" id="changelog-tab" role="tabpanel">
                                        <div class="theme-description">
                                            <h2>VERSION 2.0.0</h2>
                                            <p><em>May 27, 2021</em></p>
                                            <ul>
                                                <li>This version adds Bootstrap 5 support.</li>
                                                <li>v2.0.0 now comes with two folders inside the ZIP file &#8211; bootstrap-4 (contains the B4 version of the theme) and bootstrap-5 &#8211; contains v2.0.0 files.</li>
                                            </ul>
                                            <p><strong>main changes</strong></p>
                                            <ul>
                                                <li>updating pug/html to support new Bootstrap 5 classes, removing deprecated classes (affects 95% of the pug files)</li>
                                                <li>moving custom SCSS/CSS to B5 Utility classes (where possible)</li>
                                                <li>this version still comes with jQuery as a dependency. jQuery support will be removed in Directory v3.0.0</li>
                                                <li>updated packages: Bootstrap (5.0.1), Dropzone (5.9.2), NoUI Slider (15.1.0)</li>
                                            </ul>
                                            <h2>VERSION 1.6.1</h2>
                                            <p><em>December 15, 2020</em></p>
                                            <ul>
                                                <li><strong>fixed</strong>: small display issues in compare.html, knowledge-base.html</li>
                                                <li><strong>updated packages:</strong> Bootstrap (4.5.3)</li>
                                            </ul>
                                            <h2>VERSION 1.6.0</h2>
                                            <p><em>September 15, 2020</em></p>
                                            <ul>
                                                <li><strong>new pages:</strong> Invoice <em>(user-invoice.html)</em>, User messages<em> (user-messages.html)</em>, User messages &#8211; Conversation<em> (user-messages-detail.html)</em>, Knowledge Base <em>(knowledge-base.html)</em>,
                                                    Knowledge Base &#8211; Topic <em>(knowledge-base-topic.html)</em>, Terms &amp; Conditions <em>(terms.html)</em></li>
                                                <li><strong>updated packages:</strong> Bootstrap <em>(4.5.2)</em>, Bootstrap Select <em>(1.13.18)</em>, NoUI Slider <em>(14.6.1)</em></li>
                                            </ul>
                                            <h2>VERSION 1.5.1</h2>
                                            <p><em>June 22, 2020</em></p>
                                            <ul>
                                                <li><strong>updated packages:</strong> Bootstrap <em>(4.5.0)</em>, jQuery <em>(3.5.1)</em>, Bootstrap Select <em>(1.13.17)</em></li>
                                            </ul>
                                            <p>&nbsp;</p>
                                            <h2>VERSION 1.5</h2>
                                            <p><em>May 5, 2020</em></p>
                                            <ul>
                                                <li><strong>improved:</strong> In addition to HTML files, this theme now comes with Pug file sources, and changes in the Gulp workflow. Pug compilation is now the recommended way of work with this theme. Pug
                                                    sources use includes and mixins to avoid repetitive code (navbar, footer, cards). If you change the navbar, the change will be effective in all your pages, unlike when developing with HTML, where you
                                                    would need to do all the changes in every file.
                                                </li>
                                                <li><strong>changes:</strong></li>
                                                <li>pages folder now renamed to html</li>
                                                <li>to run previously default `gulp` command, use `gulp dev-html`</li>
                                                <li>`gulp build` and `gulp build-html` now compile everything into `build` folder, previously `dist`</li>
                                            </ul>
                                            <h2>VERSION 1.4.2</h2>
                                            <p><em>April&nbsp;6,&nbsp;2020</em></p>
                                            <ul>
                                                <li><strong>new pages:</strong> User profile <em>(user-profile.html)</em>, User account <em>(user-account.html)</em>, Personal Info &#8211; demo forms <em>(user-personal.html)</em>, Password &amp; security &#8211;
                                                    demo forms <em>(user-security.html)</em>, 404 page <em>(404.html) </em></li>
                                                <li><strong>improved:</strong> Moved the inlined script where var tileLayers were defined to js/map-layers.js</li>
                                                <li><strong>updated:</strong> Gulp plugins (gulp-clean-css, gulp-rename), Smooth Scroll</li>
                                            </ul>
                                            <h2>VERSION 1.4.1</h2>
                                            <p><em>February 12, 2020</em></p>
                                            <ul>
                                                <li>updated: Switched the Map tiles provider from Wikimedia to OpenStreetMaps + Carto (affected files: js/map-detail.js, js/map-category.js). Wikimedia, unfortunately, stopped working.</li>
                                            </ul>
                                            <h2>VERSION 1.4.0</h2>
                                            <p><em>December 20, 2019</em></p>
                                            <ul>
                                                <li><strong>new pages:</strong> Real estate home page (index-4.html), comparison/wishlist (compare.html), Team (team.html)</li>
                                                <li><strong>improved:</strong> added 5 additional pre-made CSS colour variants (see pages/css folder)</li>
                                                <li><strong>updated plugins:</strong> Bootstrap (4.4.1), Bootstrap Select (1.13.12), NoUI Slider (14.1.1), Swiper (4.5.1)</li>
                                            </ul>
                                            <h2>Version 1.3.0</h2>
                                            <p><em>September 9, 2019</em></p>
                                            <ul>
                                                <li><strong>new pages:</strong> Travel home page <em>(index-3.html)</em></li>
                                                <li><strong>improved:</strong> added hover animations to cards using a new .hover-animate utility class</li>
                                                <li><strong>updated plugins:</strong> Bootstrap Select (1.13.10), jQuery (3.4.1), LeafletJS (1.5.1), NoUI Slider (14.0.2), PrismJS (1.17.1), SmoothScroll (16.1.0), Swiper (4.5.0)</li>
                                                <li><strong>updated development workflow:</strong> upgraded to Gulp 4 (4.0.2), switched from CSS Nano Gulp plugin to Gulp Clean CSS (4.2.0). Affected files: <em>gulpfile.js, package.json</em></li>
                                            </ul>
                                            <h2><strong>Version 1.2.0</strong></h2>
                                            <p><em>April 24, 2019</em></p>
                                            <ul>
                                                <li><strong>new pages</strong>: User Bookings <em>(user-grid.html)</em>, Booking detail <em>(user-booking-detail.html)</em>, Booking process <em>(4 pages: user-booking-1.html &#8211; user-booking-4.html, _booking.scss)</em>,
                                                    Host view <em>(user-list.html)</em></li>
                                                <li><strong>improved:</strong> &#8211; added .icon-rounded-sm modifier classes <em>(_icons.scss)</em></li>
                                                <li><strong>improved:</strong> &#8211; better alert theming <em>(_variables.scss)</em></li>
                                                <li><strong>updated:</strong> Font Awesome to v 5.8.1</li>
                                            </ul>
                                            <h2>Version 1.1.0</h2>
                                            <p><em>March 7, 2019</em></p>
                                            <ul>
                                                <li><strong>new pages:</strong> Sign in (login.html), Sign up <em>(signup.html)</em>, Add new listing form <em>(6 pages: user-add-0.html &#8211; user-add-5.html)</em></li>
                                                <li><strong>improved:</strong> added SVG logo for demo purposes</li>
                                                <li><strong>improved:</strong> to avoid CORS issues when viewing using file:// protocol, using the Bootstrapious.com URL for the SVG sprite</li>
                                                <li><strong>fixed:</strong> left/right arrows in Magnific popup <em>(affected files: custom/_magnificpopup.scss, new file custom/_functions.scss)</em></li>
                                                <li><strong>fixed:</strong> SCSS customization &#8211; improved styling possibilities for brand colours, added few missing !default flags, new variable for .nav-link in navbar &#8211; $navbar-link-size <em>(affected files: custom/_variables.scss, user/_user-variables.scss)</em></li>
                                            </ul>
                                            <h2>Version 1.0.0</h2>
                                            <p><em>February 14, 2019</em></p>
                                            <ul>
                                                <li>Initial release</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="summary entry-summary"> </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-none d-lg-block pl-xs-0 pl-lg-5">
                        <div class="d-flex align-items-center mb-3">
                            <h2 class="fs-20 mb-0">License Options</h2> <span class="fs-13 ml-auto"> <a href="/page/legal/licenses">Full details &rarr;</a> </span> </div>
                        <form class="mb-4">
                            <div class="form-group mb-4 switch_price_prod" data-type="Standard License">
                                <div class="custom-control custom-control-license"> <input class="custom-control-input" id="licenseSingle" name="license" type="radio" checked> <label class="custom-control-label d-flex align-items-center" for="licenseSingle"> <div class="mr-auto"> <h6 class="mb-0">Standard GNU 2.0</h6> <span class="fs-13 text-gray-soft">Country ranked 100 or bellow single Site</span> </div><div class="d-flex align-items-center ml-auto"> <strong class="fs-18 mr-1">$</strong> <strong class="h2">0</strong> </div></label>                                    </div>
                            </div>
                            <div class="form-group mb-4 switch_price_prod" data-type="Multisite License">
                                <div class="custom-control custom-control-license"> <input class="custom-control-input" id="licenseMultisite" name="license" type="radio"> <label class="custom-control-label d-flex align-items-center" for="licenseMultisite"> <div class="mr-auto"> <h6 class="mb-0">Commercial</h6> <span class="fs-13 text-gray-soft">Developed naion with paying users</span> </div><div class="d-flex align-items-center ml-auto"> <strong class="fs-18 mr-1">$</strong> <strong class="h2">149</strong> </div></label>                                    </div>
                            </div>
                            <div class="form-group mb-4 switch_price_prod" data-type="Extended License">
                                <div class="custom-control custom-control-license"> <input class="custom-control-input" id="licenseExtended" name="license" type="radio"> <label class="custom-control-label d-flex align-items-center" for="licenseExtended"> <div class="mr-auto"> <h6 class="mb-0">Enterprise</h6> <span class="fs-13 text-gray-soft">Five eyes with paying users</span> </div><div class="d-flex align-items-center ml-auto"> <strong class="fs-18 mr-1">$</strong> <strong class="h2">590</strong> </div></label>                                    </div>
                            </div>
                        </form>
                        <form action="/cart" method="POST" class=" btn-block"> <input type="hidden" js-license-type="license_type" name="license_type" value="Standard License" /> <input type="hidden" name="add-to-cart" value="17532" /> <button type="submit" class="btn btn-brand btn-block btn-checkout"> <span class="btn-text">Add to cart</span></button>                            </form> <a class="btn btn-outline-brand btn-block mb-4 ml-0" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=17532">Live preview</a>
                        <div class="theme-purchases">
                            <div class="theme-purchases__item">
                                <a class="theme-purchases__item__inner text-center" data-toggle="tab" href="#reviews-tab" role="tab" js-handle="review-toggler">
                                    <ul class="rating justify-content-center">
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                        <li class="rating__item rating__item--active"></li>
                                    </ul>
                                    <p>5.00/5 (18 reviews)</p>
                                </a>
                                <div class="theme-purchases__item__inner text-center">
                                    <h5 class="mb-0"><i class="bootstrap-themes-icon-cart"></i>973</h5>
                                    <p>Purchases</p>
                                </div>
                            </div>
                            <div class="theme-purchases__item">
                                <div class="theme-purchases__item__inner px-0">
                                    <ul class="guarantees">
                                        <li> <i class="bootstrap-themes-icon-check-circle"></i>Reviewed by the Bootstrap team</li>
                                        <li><i class="bootstrap-themes-icon-check-circle"></i><a href="https://themes.zendesk.com/hc/en-us/articles/360000006291-How-do-I-get-help-with-the-theme-I-purchased-">6 months technical support</a></li>
                                        <li><i class="bootstrap-themes-icon-check-circle"></i>100% money back guarantee</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="theme-description__list">
                            <div class="theme-description__list__item"><span class="theme-description__item__title">GovNet Versions</span><span  id="govenetversions">v5.0.1</span></div>
                            <div class="theme-description__list__item"><span class="theme-description__item__title">Released</span><span  id="createdon">2 years ago</span></div>
                            <div class="theme-description__list__item"><span class="theme-description__item__title">Updated</span><span id="updatedon">1 month ago</span></div>
                            <div class="theme-description__list__item"><span class="theme-description__item__title">Version</span><span id="version"></span></div>
                            <div class="theme-description__list__item"><span class="theme-description__item__title">Tags</span> <a href=""><span id="tags">Loading...</span></a> </div>
                            <div class="theme-description__list__item align-items-center"><span class="theme-description__item__title">Questions?</span><a class="btn btn-xs btn-outline-brand" href="mailto:hello@opengov.site" target="_blank">Contact Us</a></div>
                            <div class="theme-description__list__item">
                                <a class="profile-author" href="https://themes.getbootstrap.com/store/bootstrapious">
                                    <div class="profile-author__logo"> <img class="profile-author__img" src="https://themes.getbootstrap.com/wp-content/uploads/2018/06/btstrapious-square-300x300.png" alt=""> </div>
                                    <div class="profile-author__author__description">
                                        <p>Created by</p>
                                        <h6 class="profile-logo__author__title">Bootstrapious</h6>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="theme-cards-holder mt-5 pt-5" style="border-top: 1px solid #D5DCE5; border-bottom-width: 0; margin-bottom: -60px;">
                    <div class="theme-cards__heading">
                        <div>
                            <h5 class="theme-cards__title">E-Commerce &amp; Retail Themes</h5>
                            <p class="text-gray-soft">Related themes in the same category.</p>
                        </div><a class="theme-cards__heading__button btn btn-outline-brand btn-sm" href="https://themes.getbootstrap.com/product-category/ecommerce-retail/">View All</a> </div>
                    <ul class="row">
                        <li class="col-md-4 col-6">
                            <div class="theme-card">
                                <div class="theme-card__body"> <a class="d-block" href="https://themes.getbootstrap.com/product/finder-directory-listings-template-ui-kit/"> <img width="400" height="300" src="https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-400x300.jpg" class="theme-card__img wp-post-image" alt="" srcset="https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-400x300.jpg 400w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-800x600.jpg 800w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot.jpg 1200w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-768x576.jpg 768w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-600x450.jpg 600w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-200x150.jpg 200w, https://themes.getbootstrap.com/wp-content/uploads/2021/06/screenshot-540x405.jpg 540w" sizes="(max-width: 400px) 100vw, 400px"/> </a>                                    <a class="theme-card__body__overlay btn btn-brand btn-sm" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=92316">Live preview</a> </div>
                                <div class="theme-card__footer">
                                    <div class="theme-card__footer__item"><a class="theme-card__title mr-1" href="https://themes.getbootstrap.com/product/finder-directory-listings-template-ui-kit/">Finder – Directory &#038; Listings Template + UI Kit</a>
                                        <p class="theme-card__info">
                                            <ul class="prod_cats_list">
                                                <li><a href="https://themes.getbootstrap.com/product-category/ecommerce-retail/">E-Commerce &amp; Retail</a></li>
                                            </ul>
                                        </p>
                                    </div>
                                    <div class="theme-card__footer__item">
                                        <p class="theme-card__price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>49.00</span>
                                        </p>
                                        <ul class="rating">
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-6">
                            <div class="theme-card">
                                <div class="theme-card__body"> <a class="d-block" href="https://themes.getbootstrap.com/product/cartzilla-bootstrap-e-commerce-template-ui-kit/"> <img width="400" height="300" src="https://themes.getbootstrap.com/wp-content/uploads/2019/10/screenshot-3-400x300.jpg" class="theme-card__img wp-post-image" alt="" srcset="https://themes.getbootstrap.com/wp-content/uploads/2019/10/screenshot-3-400x300.jpg 400w, https://themes.getbootstrap.com/wp-content/uploads/2019/10/screenshot-3-800x600.jpg 800w, https://themes.getbootstrap.com/wp-content/uploads/2019/10/screenshot-3.jpg 1200w, https://themes.getbootstrap.com/wp-content/uploads/2019/10/screenshot-3-768x576.jpg 768w, https://themes.getbootstrap.com/wp-content/uploads/2019/10/screenshot-3-600x450.jpg 600w, https://themes.getbootstrap.com/wp-content/uploads/2019/10/screenshot-3-200x150.jpg 200w, https://themes.getbootstrap.com/wp-content/uploads/2019/10/screenshot-3-540x405.jpg 540w" sizes="(max-width: 400px) 100vw, 400px"/> </a>                                    <a class="theme-card__body__overlay btn btn-brand btn-sm" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=35287">Live preview</a> </div>
                                <div class="theme-card__footer">
                                    <div class="theme-card__footer__item"><a class="theme-card__title mr-1" href="https://themes.getbootstrap.com/product/cartzilla-bootstrap-e-commerce-template-ui-kit/">Cartzilla – Multipurpose eCommerce Template</a>
                                        <p class="theme-card__info">
                                            <ul class="prod_cats_list">
                                                <li><a href="https://themes.getbootstrap.com/product-category/ecommerce-retail/">E-Commerce &amp; Retail</a></li>
                                            </ul>
                                        </p>
                                    </div>
                                    <div class="theme-card__footer__item">
                                        <p class="theme-card__price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>49.00</span>
                                        </p>
                                        <ul class="rating">
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-6">
                            <div class="theme-card">
                                <div class="theme-card__body"> <a class="d-block" href="https://themes.getbootstrap.com/product/shopper/"> <img width="400" height="300" src="https://themes.getbootstrap.com/wp-content/uploads/2019/11/uinvgh78-400x300.jpg" class="theme-card__img wp-post-image" alt="" srcset="https://themes.getbootstrap.com/wp-content/uploads/2019/11/uinvgh78-400x300.jpg 400w, https://themes.getbootstrap.com/wp-content/uploads/2019/11/uinvgh78-600x450.jpg 600w, https://themes.getbootstrap.com/wp-content/uploads/2019/11/uinvgh78-200x150.jpg 200w, https://themes.getbootstrap.com/wp-content/uploads/2019/11/uinvgh78-540x405.jpg 540w, https://themes.getbootstrap.com/wp-content/uploads/2019/11/uinvgh78-1200x900.jpg 1200w" sizes="(max-width: 400px) 100vw, 400px"/> </a>                                    <a class="theme-card__body__overlay btn btn-brand btn-sm" target="_blank" href="https://themes.getbootstrap.com/preview/?theme_id=37702">Live preview</a> </div>
                                <div class="theme-card__footer">
                                    <div class="theme-card__footer__item"><a class="theme-card__title mr-1" href="https://themes.getbootstrap.com/product/shopper/">Shopper – Multipurpose E-Commerce Template</a>
                                        <p class="theme-card__info">
                                            <ul class="prod_cats_list">
                                                <li><a href="https://themes.getbootstrap.com/product-category/ecommerce-retail/">E-Commerce &amp; Retail</a></li>
                                            </ul>
                                        </p>
                                    </div>
                                    <div class="theme-card__footer__item">
                                        <p class="theme-card__price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>49.00</span>
                                        </p>
                                        <ul class="rating">
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                            <li class="rating__item rating__item--active"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                </main>
            </div>
            </div>
            </div>
            </div>
        </section>
        <script>
            //Setup url
            const URL = "https://api.opengov.site/v1/app/1";
            //Define html elements
            const description = document.getElementById("descriptionText");
            description.innerHTML = "<p>Loading...<p>";
            const title = document.getElementById("title");
            title.innerHTML = '<h1 class="d-none d-lg-block mb-3">Loading...</h1>';
            const mainimage = document.getElementById("mainimage");
            mainimage.innerHTML = '<p>Loading image...</p>';
            const tags = document.getElementById("tags");
            tags.innerHTML = 'Loading tags...';
            const version = document.getElementById("version");
            version.innerHTML = 'Loading version...';
            const govenetversions = document.getElementById("govenetversions");
            govenetversions.innerHTML = 'Loading version...';

            //Api calls
            fetch(URL).then((response) => response.json()).then(apps => {
                title.innerHTML = getTitle(apps);
                description.innerHTML = getDescription(apps);
                mainimage.innerHTML = getMainImage(apps);
                tags.innerHTML = getTags(apps);
                version.innerHTML = getVersion(apps);
                govenetversions.innerHTML = getGovNetVersions(apps);
            });
            //Functions
            const getDescription = (apps) => {
              const description = apps.map((app) => `${app.description} `).join("\n");

              return `${description}`;
            };
            const getTitle = (apps) => {
              const title = apps.map((app) => `<h1 class="d-none d-lg-block mb-3">${app.title}</h1>`).join("\n");

              return `${title}`;
            };
            const getMainImage = (apps) => {
              const mainimage = apps.map((app) => `<a href="/app/${app.id}"><img width="1200" height="900" src="${app.image}" class="attachment-large_crop size-large_crop" /></a>`).join("\n");
              return `${mainimage}`;
            };
            const getTags = (apps) => {
              const tags = apps.map((app) => `${app.tags}`).join("\n");
              return `${tags}`;
            };
            const getVersion = (apps) => {
              const version = apps.map((app) => `${app.version}`).join("\n");
              return `${version}`;
            };
            const getGovNetVersions = (apps) => {
              const goveNetVersions = apps.map((app) => `${app.govenet_versions}`).join("\n");
              return `${goveNetVersions}`;
            };
        </script>
@endsection
