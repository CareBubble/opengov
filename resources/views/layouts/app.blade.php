<!DOCTYPE html>
<!-- saved from url=(0032)https://themes.getbootstrap.com/ -->
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="https://themes.getbootstrap.com/xmlrpc.php" />
        <link rel="apple-touch-icon" sizes="180x180" href="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/fav/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/fav/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/fav/favicon-16x16.png" />
        <meta name="msapplication-config" content="https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/images/fav/browserconfig.xml" />
        <!-- This site is optimized with the Yoast SEO plugin v12.7.1 - https://yoast.com/wordpress/plugins/seo/ -->
        <meta
            name="description"
            content="Bootstrap Themes is a collection of the best templates and themes curated by Bootstrap’s creators. Our collection of templates include themes to build an admin, dashboard, landing page, e-commerce site, application, and more."
        />
        <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
        <link rel="canonical" href="https://themes.getbootstrap.com/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Bootstrap Themes Built &amp; Curated by the Bootstrap Team." />
        <meta
            property="og:description"
            content="Bootstrap Themes is a collection of the best templates and themes curated by Bootstrap’s creators. Our collection of templates include themes to build an admin, dashboard, landing page, e-commerce site, application, and more."
        />
        <meta property="og:url" content="https://themes.getbootstrap.com/" />
        <meta property="og:site_name" content="Bootstrap Themes" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta
            name="twitter:description"
            content="Bootstrap Themes is a collection of the best templates and themes curated by Bootstrap’s creators. Our collection of templates include themes to build an admin, dashboard, landing page, e-commerce site, application, and more."
        />
        <meta name="twitter:title" content="Bootstrap Themes Built &amp; Curated by the Bootstrap Team." />
        <script async="" src="./theme_files/37107ccd183a988edab652f4b.js"></script>
        <!-- / Yoast SEO plugin. -->
        <link rel="dns-prefetch" href="https://s.w.org/" />
        <link rel="alternate" type="application/rss+xml" title="Bootstrap Themes » Feed" href="https://themes.getbootstrap.com/feed/" />
        <link rel="alternate" type="application/rss+xml" title="Bootstrap Themes » Comments Feed" href="https://themes.getbootstrap.com/comments/feed/" />
        <link rel='stylesheet' id='dokan-fontawesome-css'  href='https://themes.getbootstrap.com/wp-content/plugins/dokan-lite/assets/vendors/font-awesome/font-awesome.min.css?ver=2.9.27' type='text/css' media='all' />
        <link rel='stylesheet' id='dokan-theme-skin-css'  href='https://themes.getbootstrap.com/wp-content/themes/dokan/assets/css/skins/purple.css' type='text/css' media='all' />
        <script src="./theme_files/wp-emoji-release.min.js" type="text/javascript" defer=""></script>
        <link rel='stylesheet' id='wp-block-library-css'  href='https://themes.getbootstrap.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3' type='text/css' media='all' />
        <link rel='stylesheet' id='wc-block-style-css'  href='https://themes.getbootstrap.com/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=2.4.5' type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-layout-css'  href='https://themes.getbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.8.1' type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-smallscreen-css'  href='https://themes.getbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=3.8.1' type='text/css' media='only screen and (max-width: 768px)' />
        <link rel='stylesheet' id='woocommerce-general-css'  href='https://themes.getbootstrap.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=3.8.1' type='text/css' media='all' />
        <script type='text/javascript' src='https://themes.getbootstrap.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>

        <!-- The filemtime is to append a timestamp for the last time the stylesheet was updated to automate cache busting from CloudFlare -->
      <link rel='stylesheet' href='https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/style.css?ver=1590611604' />
    </head>
    <body class="home blog woocommerce theme-dokan woocommerce-js dokan-theme-dokan">
        <!-- <div id="page" class="hfeed site"> -->
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a href="/" class="navbar-brand">{{ config('app.name', 'Laravel') }}</a>
                <div class="d-flex ml-auto">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#globalNavbar" aria-controls="globalNavbar" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="globalNavbar">
                    <form class="form-inline form-navbar my-2 my-lg-0 order-2" action="https://themes.getbootstrap.com/shop/"><input class="form-control" name="s" type="text" placeholder="Search" /></form>
                    <ul class="navbar-nav mr-auto order-1">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" href="https://themes.getbootstrap.com/#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <div class="navbar-collapse navbar-top-collapse">
                                    <ul id="menu-top-menu" class="nav navbar-nav">
                                        <li id="menu-item-601" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-601">
                                            <a title="Admin &amp; Dashboard" href="https://themes.getbootstrap.com/product-category/admin-dashboard/">Admin &amp; Dashboard</a>
                                        </li>
                                        <li id="menu-item-603" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-603">
                                            <a title="Landing &amp; Corporate" href="https://themes.getbootstrap.com/product-category/landing-corporate/">Landing &amp; Corporate</a>
                                        </li>
                                        <li id="menu-item-1537" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1537">
                                            <a title="Application" href="https://themes.getbootstrap.com/product-category/application/">Application</a>
                                        </li>
                                        <li id="menu-item-602" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-602">
                                            <a title="E-Commerce &amp; Retail" href="https://themes.getbootstrap.com/product-category/ecommerce-retail/">E-Commerce &amp; Retail</a>
                                        </li>
                                        <li id="menu-item-1538" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1538">
                                            <a title="Portfolio &amp; Blog" href="https://themes.getbootstrap.com/product-category/portfolio-blog/">Portfolio &amp; Blog</a>
                                        </li>
                                        <li id="menu-item-1539" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1539">
                                            <a title="Specialty" href="https://themes.getbootstrap.com/product-category/specialty/">Specialty</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="/about">Why Open Gov?</a></li>
                        <li class="nav-item"><a class="nav-link" href="/guide">The Guide</a></li>
                    </ul>
                    @guest
                      <ul class="navbar-nav d-none d-lg-flex ml-2 order-3">
                          <li class="nav-item"><a class="nav-link" href="/login">Sign in</a></li>
                          <li class="nav-item"><a class="nav-link" href="/register">Sign up</a></li>
                      </ul>
                      <ul class="navbar-nav d-lg-none">
                          <li class="nav-item-divider"></li>
                          <li class="nav-item"><a class="nav-link" href="/login">Sign in</a></li>
                          <li class="nav-item"><a class="nav-link" href="/register">Sign up</a></li>
                      </ul>
                    @else
                      <ul class="navbar-nav d-none d-lg-flex ml-2 order-3">
                        @if(isset($_SESSION['cart']) AND $_SESSION['cart'][0] != null)
                            <li class="nav-item mr-3"><a class="btn btn-brand btn-sm navbar-btn" href="/cart"><i class="bootstrap-themes-icon-cart"></i><span class="navbar__btn-quantity mr-1">1</span></a></li>
                        @endif
                        <li class="nav-item dropdown show">
                            <a class="nav-link nav-link--user dropdown-toggle" id="navbarDropdownMenuLink" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <div class="nav-link--user__initials">
                                    JV
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarCustomerDropdownMenuLink">
                                <a class="dropdown-item" href="https://themes.getbootstrap.com/my-account/edit-account/">Account</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out</a>
                            </div>
                        </li>
                      </ul>
                    @endguest
                </div>
            </div>
        </nav>
        <main id="main" class="site-main main">
            @yield('content');
        </main>
        <!-- #main .site-main -->
        <footer class="footer">
            <div class="container">
                <div class="footer__inner">
                    <div class="footer__item">
                        <form
                            action="https://getbootstrap.us13.list-manage.com/subscribe/post?u=e3428dd6b8fda73bc5ba8b6e6&amp;id=198881a019"
                            method="post"
                            id="mc-embedded-subscribe-form"
                            name="mc-embedded-subscribe-form"
                            target="_blank"
                            novalidate=""
                        >
                            <div class="d-md-flex justify-content-between align-items-center">
                                <div class="form-group">
                                    <h5 class="mb-1">Get new apps in your inbox!</h5>
                                    <div class="form-text mt-0">New themes or big discounts. Never spam.</div>
                                </div>
                                <div id="signup_footer" class="d-flex align-items-start">
                                    <div class="form-group w-100 mr-2">
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e3428dd6b8fda73bc5ba8b6e6_198881a019" tabindex="-1" value="" /></div>
                                        <input id="mce-EMAIL" class="footer__email-input form-control form-control--muted" name="EMAIL" type="email" aria-describedby="footerEmail" placeholder="Email address" />
                                    </div>
                                    <input class="btn btn-brand" type="submit" value="Subscribe" name="subscibe" id="mc-embedded-subscribe" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="footer__item d-lg-flex justify-content-lg-between align-items-lg-center">
                        <ul id="menu-seller-footer" class="nav sub-nav footer__sub-nav">
                            <li id="menu-item-1333" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1333"><a title="Help Center" href="https://themes.zendesk.com/hc/en-us">Help Center</a></li>
                            <li id="menu-item-1335" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1335"><a title="Terms of Service" href="https://themes.getbootstrap.com/terms/">Terms of Service</a></li>
                            <li id="menu-item-1334" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1334"><a title="Licenses" href="https://themes.getbootstrap.com/licenses/">Licenses</a></li>
                            <li id="menu-item-49443" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49443"><a title="Sell Themes" href="https://themes.getbootstrap.com/sell/">Sell Themes</a></li>
                        </ul>
                        <p class="hidden-sm-down d-none d-lg-block">Trying to redownload a theme? Use our <a href="https://themes.getbootstrap.com/redownload/">redownload page.</a></p>
                    </div>
                </div>
            </div>
        </footer>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        <script type="text/javascript" src="https://themes.getbootstrap.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70"></script>
        <script type='text/javascript' src='https://themes.getbootstrap.com/wp-content/plugins/dokan-lite/assets/vendors/magnific/jquery.magnific-popup.min.js?ver=2.9.27'></script>
        <script type='text/javascript' src='https://themes.getbootstrap.com/wp-content/plugins/dokan-lite/assets/js/login-form-popup.js?ver=1580165288'></script>
        <script type='text/javascript' src='https://themes.getbootstrap.com/wp-includes/js/wp-embed.min.js?ver=5.3'></script>
        <div id="yith-wcwl-popup-message" style="display: none;"><div id="yith-wcwl-message"></div></div>
        <script src='https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/javascript/Chart.min.js'></script>
        <script src='https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/javascript/Chart.bundle.min.js'></script>
        <script src='https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/javascript/tether.min.js'></script>
        <script src='https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/javascript/popper.min.js'></script>
        <script src='https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/javascript/bootstrap.min.js'></script>
        <script src='https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/javascript/lazysizes.min.js'></script>
        <!-- The filemtime is to append a timestamp for the last time the stylesheet was updated to automate cache busting from CloudFlare -->
 <script src='https://themes.getbootstrap.com/wp-content/themes/bootstrap-marketplace/assets/javascript/scripts.js?ver=1617762869'></script>
    </body>
</html>
